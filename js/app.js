(function ($) {
    $(function(){
        $('.header__menu-btn').click(function(){
            $('body').toggleClass('pull-left');
            $('.header__menu').toggleClass('header__menu_visible');
        })
    })
})(jQuery);